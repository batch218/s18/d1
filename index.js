// console.log("Hello world");

// function printInfo(){
// 	let nickname = prompt("Enter your nickname");
// 	console.log("Hi, " + nickname);
// };

// printInfo();

				// Parameter
function printName(firstName) {
	console.log("My Name is " + firstName);
};
printName("Juana"); //argument
printName("John");
printName("Cena");
// Now we have a resable function/ reusable task but could have a different output based on what value to process, with the help of ..

// [SECTION] Parameters and Arguments

	// "firstName" is called parameter
	// A "parameter" acts as named variable/ container that exists only inside a function 
	// It is used to store information that is provided to a function when it is called/ invoke

// Argument 
	// "Juana", "John", "Cena" the information/data provided directly into the function is called "argument".
	// Values passed when invoking a function are called arguments.
let sampleVariable = "Inday";

printName(sampleVariable);

// Variable can also be passed to as an argument

// -----------------------------

function checkDivisibilityBy8(num){
	let remainder = num % 8 ;
	console.log("The remainder of " +num+ " divided by 8 is: "+ remainder);
	let isDivisibilityBy8 = remainder === 0; // will store a value true/ false
	console.log("Is "+num+ " divisible by 8 ?");
	console.log(isDivisibilityBy8);
};
checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// [SECTION] Function as argument


function argumentFunction(){
	console.log("This function was passed as an argument before the message was printed.");
};
function invokeFunction(argumentFunction){
	argumentFunction();
};

invokeFunction(argumentFunction);
console.log(argumentFunction);
// ==============================


// [SECTION] Using Multiple Parameters 

function createFullName(firstName, middleName, lastName) {
	console.log("My full name is " + firstName + " "+ middleName + " " + lastName);
}
createFullName("Romulo", "Longaza", "Baal");


// Using variables as an argument
let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName,middleName,lastName);


function getDifferenceOf8Minu4(numA, numB){
	console.log("Difference: " + (numA - numB));

}
getDifferenceOf8Minu4(8,4);
// getDifferenceOf8Minu4(4,8); // This will result to logical error

// [SECTION] ReTURN statement
 
// The "return" statement allows us to output a value from a function to be passed to the line/block of code that invoked/called
function returnFullName(firstName,middleName, lastName ) {
	// return firstName + " " + middleName + " " + lastName;
	// This line of code will not be printed
	console.log("This is printed inside a function");
	let fullName = firstName + " " + middleName + " " + lastName;
	return fullName;

}
let completeName = returnFullName("Jeffrey", "Smith", "Jordan");
console.log(completeName);

console.log("I am " + completeName);

console.log("---------------------------")

function printPlayerInfo(userName, level, job) {
	console.log("Username: " + userName);
	console.log("Level: " + level);
	console.log("Job: "+ job);
}
let user1 = printPlayerInfo ("bosing", "Senior", "Programmer");
console.log(user1);